<?php

use App\Models\Article;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// App version
$app->get('/', function () use ($app) {
    return $app->version();
});

// Health check URL
$app->get('health', function () use ($app) {
    return 'OK';
});

// Application URLs

$app->get('articles', 'ArticlesController@getAll');

$app->get('articles/{id}', 'ArticlesController@getOne');

$app->post('articles', 'ArticlesController@create');

$app->put('articles/{id}', 'ArticlesController@update');

$app->delete('articles/{id}', 'ArticlesController@deleteOne');

//$app->delete('articles', 'ArticlesController@deleteAll');
