<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticlesController extends Controller
{
    public function getAll()
    {
        return Article::orderBy('created_at', 'DESC')->get();
    }

    public function getOne($id)
    {
        return Article::findOrFail($id);
    }

    public function create(Request $request)
    {
        return Article::create([
            'title'     => $request->input('title'),
            'content'   => $request->input('content'),
        ]);
    }

    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);

        $article->title = $request->input('title');
        $article->content = $request->input('content');

        $article->save();

        return $article;
    }

    public function deleteOne($id)
    {
        return Article::destroy($id);
    }
}
